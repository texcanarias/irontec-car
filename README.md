# irontec-car
Sistema basado en Symfony4 y angular6 para la lectura de datos de un vehículo.

#Instalación
Se cuenta tanto conun servidor PHP7 como un servidor mysql instalado en la máquina local.

##Pasos
+ Crear una database con un usuario y constraseña.
+ Volcar los datos de ejemplo en ella. Fichero 'irontec-car.sql'
+ Ejecutar 'composer install' para cargar las dependencias
+ Modificar el fichero '.env' para especificar usuario, contraseña y base de datos
+ Ejecutar php bin/console server:run para realizar las pruebas del servidor
+ Entrar en el directorio angular6 'cd angular6'
+ Ejecutar 'npm install' para cargar las depedencias
+ Ejecutar 'ng serve --open' para realizar las pruebas en el lado del cliente