import { Component, OnInit } from '@angular/core';

import { Car } from '../models/car';

import { CarService } from '../services/car.service'

@Component({
  selector: 'app-cars',
  templateUrl: './cars.component.html',
  styleUrls: ['./cars.component.css']
})
export class CarsComponent implements OnInit {
  car: Car = new Car();

  constructor(private carService : CarService) { 
  }

  ngOnInit() {   
    this.getItem();
  }

  getItem():void{
    this.carService.getItem(1)
      .subscribe(item => {this.car = this.JsonToObject(item);});
  }

  JsonToObject(item):Car{
    item = JSON.parse(item);
    let newCar:Car = new Car();
    newCar.id = item.id;
    newCar.color = item.color;
    newCar.foto = item.foto;
    newCar.kilometros = item.kilometros;
    newCar.matricula = item.matricula;
    newCar.propietario = item.propietario;
    return newCar;
  }
}
