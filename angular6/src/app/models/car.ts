export class Car{
    id: number;
    matricula: string;
    color: number;
    kilometros: number;
    propietario: string;
    foto: string;

    constructor(){}


    getColor():string{
        let color:string = "";
        switch(this.color){
            case 1:
                color = "blanco";
            break;
            case 2:
                color = "gris";
            break;
            case 3:
                color = "rojo";
            break;
        }
        return color;
    }

    existFoto():boolean{
        if("" != this.foto){
            return true;
        }
        return false;
    }
}