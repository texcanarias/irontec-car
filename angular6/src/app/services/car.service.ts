import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';

import { catchError, map, tap } from 'rxjs/operators';

import { Car } from '../models/car';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})

export class CarService {

  private URLServer: string = "http://127.0.0.1:8000/api/v1/cars";

  constructor(private http: HttpClient) { 
  }

  getItem(id: number): Observable<Car> {
    const url = `${this.URLServer}/${id}`;
    return this.http.get<Car>(url).pipe(
      catchError(this.handleError<Car>(`getItem id=${id}`))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
  
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

}
