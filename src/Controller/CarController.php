<?php

// src/Controller/LuckyController.php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use App\Entity\Coche;

class CarController extends Controller {

    public function getCars($id) {
        try {
            $car = $this->getItem($id);
        } catch (Symfony\Component\Config\Definition\Exception\Exception $e) {
            return new JsonResponse(Response::HTTP_NOT_FOUND);
        }

        if (!$car) {
            return new JsonResponse(Response::HTTP_INTERNAL_SERVER_ERROR);
        }


        $response =  new JsonResponse($this->serializarItem($car), Response::HTTP_OK);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }

    private function getItem($id) {
        try {
            $car = $this->getDoctrine()
                    ->getRepository(Coche::class)
                    ->find($id);
        } catch (Symfony\Component\Config\Definition\Exception\Exception $e) {
            throw ("Error 500");
        }
        return $car;
    }

    private function serializarItem($car) {
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $serializer = new Serializer($normalizers, $encoders);

        $jsonCar = $serializer->serialize($car, 'json');

        return $jsonCar;
    }

    public function putCars() {
        $response =  new JsonResponse(array(), Response::HTTP_OK);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        return $response;
    }

}
